<?php
/**
 * native扫码支付demo
 * ====================================================
 * 模式一流程：
 * 1、组装包含支付信息的url，生成二维码
 * 2、用户扫描二维码，进行支付
 * 3、确定支付之后，微信服务器会回调预先配置的回调地址，在【微信开放平台-微信支付-支付配置】中进行配置
 * 4、在接到回调通知之后，用户进行统一下单支付，并返回支付信息以完成支付（见：native_model1_call.php）
 * 5、支付完成之后，微信服务器会通知支付成功
 * 6、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
 */
include_once "./lib/WxPay.Api.php";

//模式一
$nativeLink = new NativeLink();
$nativeLink->setParameter('product_id', "238");
$product_url = $nativeLink->getUrl();

$shortUrl = new ShortUrl();
$shortUrl->setParameter('long_url', $product_url);
$url1 = $shortUrl->getShortUrl();

?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>微信native扫码支付</title>
</head>
<body>
    <h1>模式一</h1>
    <div align="center" id="qrcode_mode1"></div>

    <script type="text/javascript" src="./qrcode.js"></script>
    <script type="text/javascript">
        var flag1 = "<?php echo $url1; ?>";
        if ($flag1) {
            //参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
            var qr = qrcode(10, 'M');
            qr.addData(url);
            qr.make();

            var dom=document.createElement('DIV');
            dom.innerHTML = qr.createImgTag();

            var element=document.getElementById("qrcode_mode1");
            element.appendChild(dom);
        }
    </script>
</body>
</html>

