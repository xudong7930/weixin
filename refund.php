<?php
/**
 * 微信退款
 * 证书必须是绝对路径
 */
include_once "./lib/WxPay.Api.php";

//以下数组是必须的参数
$data = [
    'out_trade_no' => "20160829155960",  //订单号
    'out_refund_no' => "AK117".time(), //退款单号
    'total_fee' => 100, //订单总金额
    'refund_fee' => 100, //退款金额
    'op_user_id'=>"1337896901" //商户号
];

$input = new Refund();
$input->setParameter('out_trade_no', $data['out_trade_no']);
$input->setParameter('total_fee', $data['total_fee']);
$input->setParameter('refund_fee', $data['refund_fee']);
$input->setParameter('out_refund_no', $data['out_refund_no']);
$input->setParameter('op_user_id', $data['op_user_id']);

$response = $input->getResult();

if ($response['result_code'] == 'FAIL') {
    echo "退款失败";
    var_dump($response);
    return ;
}

var_dump($response);

/*返回结果:
失败:
array (size=9)
  'return_code' => string 'SUCCESS' (length=7)
  'return_msg' => string 'OK' (length=2)
  'appid' => string 'wxcf36c0b2f6305619' (length=18)
  'mch_id' => string '1337896901' (length=10)
  'nonce_str' => string 'yC8FArPSmWV9nuX5' (length=16)
  'sign' => string '2E118F0B2E74100335A93B4F5C29073A' (length=32)
  'result_code' => string 'FAIL' (length=4)
  'err_code' => string 'REFUND_FEE_INVALID' (length=18)
  'err_code_des' => string '累计退款金额大于支付金额' (length=36)

成功:
array (size=18)
  'return_code' => string 'SUCCESS' (length=7)
  'return_msg' => string 'OK' (length=2)
  'appid' => string 'wxcf36c0b2f6305619' (length=18)
  'mch_id' => string '1337896901' (length=10)
  'nonce_str' => string 'xauM7xhG6VsbaCpw' (length=16)
  'sign' => string '5F8C360AFB0E6282F9C1B32F721758BA' (length=32)
  'result_code' => string 'SUCCESS' (length=7)
  'transaction_id' => string '4009722001201609012814168895' (length=28)
  'out_trade_no' => string '20160829155960' (length=14)
  'out_refund_no' => string 'AK1171472727710' (length=15)
  'refund_id' => string '2009722001201609010419488347' (length=28)
  'refund_channel' =>
    array (size=0)
      empty
  'refund_fee' => string '100' (length=3)
  'coupon_refund_fee' => string '0' (length=1)
  'total_fee' => string '100' (length=3)
  'cash_fee' => string '100' (length=3)
  'coupon_refund_count' => string '0' (length=1)
  'cash_refund_fee' => string '100' (length=3)
*/
