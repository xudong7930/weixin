<?php
/**
 * native扫码支付demo
 * ====================================================
 * 模式二流程：
 * 1、调用统一下单，取得code_url，生成二维码
 * 2、用户扫描二维码，进行支付
 * 3、支付完成之后，微信服务器会通知支付成功
 * 4、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
 */
include_once "./lib/WxPay.Api.php";

//模式二
$unifiedOrder = new UnifiedOrder();

//==========统一支付接口参数============
//必填参数
$unifiedOrder->setParameter('body', '贡献一分钱');
$unifiedOrder->setParameter('out_trade_no', "wx200773".time().mt_rand(1000,9999));
$unifiedOrder->setParameter('total_fee', "1");
$unifiedOrder->setParameter('notify_url', WxPayConf::NOTIFY_URL);
$unifiedOrder->setParameter('trade_type', "NATIVE");

//非必填参数
//$unifiedOrder->setParameter('sub_mch_id', "xxxx");
//$unifiedOrder->setParameter('device_info', "xxxx");
//$unifiedOrder->setParameter('attach', "xxxx");
//$unifiedOrder->setParameter('time_start', "xxxx");
//$unifiedOrder->setParameter('time_expire', "xxxx");
//$unifiedOrder->setParameter('goods_tag', "xxxx");
//$unifiedOrder->$unifiedOrder('product_id', "xxxx");

//获取统一支付接口结果
$unifiedOrderResult = $unifiedOrder->getResult();
$url2 = $unifiedOrderResult['code_url'];
?>


<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>微信native扫码支付</title>
</head>
<body>

<h1>模式二</h1>
<div align="center" id="qrcode_mode2"></div>

<script type="text/javascript" src="./qrcode.js"></script>
<script type="text/javascript">

    var flag2 = "<?php echo $unifiedOrderResult["code_url"] != NULL; ?>";
    if(flag2) {
        var url = "<?php echo $code_url;?>";
        //var url = "weixin://wxpay/bizpayurl?pr=tSP9gIU";

        //参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
        var qr = qrcode(10, 'M');
        qr.addData(url);
        qr.make();

        var code=document.createElement('div');
        code.innerHTML = qr.createImgTag();

        var element=document.getElementById("qrcode_mode2");
        element.appendChild(code);
    }

</script>
</body>
</html>

