<?php
/**
 * JS_API支付demo
 * ====================================================
 * 在微信浏览器里面打开H5网页中执行JS调起支付。接口输入输出数据格式为JSON。
 * 成功调起支付需要三个步骤:
 * 步骤1：网页授权获取用户openid
 * 步骤2：使用统一支付接口，获取prepay_id
 * 步骤3：使用jsapi调起支付
 */
include_once "./lib/WxPay.Api.php";

$jsApi = new JsApi();

//=========步骤1：网页授权获取用户openid============
if (!isset($_GET['code'])) {
    $url = $jsApi->createOauthUrlForCode(WxPayConf::JS_API_CALL_URL);
    Header("Location: $url");
} else {
    $code = $_GET['code'];
    $jsApi->setCode($code);
    $openid = $jsApi->getOpenid();
}

//=========步骤2：使用统一支付接口，获取prepay_id============
$unifiedOrder = new UnifiedOrder();

//==========统一支付接口参数============
//必填参数
$unifiedOrder->setParameter('openid', $openid);
$unifiedOrder->setParameter('body', '贡献一分钱');
$unifiedOrder->setParameter('out_trade_no', "wx500233".time().mt_rand(1000,9999));
$unifiedOrder->setParameter('total_fee', "1");
$unifiedOrder->setParameter('notify_url', WxPayConfig::NOTIFY_URL);
$unifiedOrder->setParameter('trade_type', "JSAPI");

//可选参数
//$unifiedOrder->setParameter('sub_mch_id', "xxxx");
//$unifiedOrder->setParameter('device_info', "xxxx");
//$unifiedOrder->setParameter('attach', "xxxx");
//$unifiedOrder->setParameter('time_start', "xxxx");
//$unifiedOrder->setParameter('time_expire', "xxxx");
//$unifiedOrder->setParameter('goods_tag', "xxxx");
//$unifiedOrder->$unifiedOrder('product_id', "xxxx");

$prepayID = $unifiedOrder->getPrepayId();

//===============步骤3：使用jsapi调起支付================
$jsApi->setPrepayId($prepayID);
$jsApiParameters = $jsApi->getParameters(); //json格式: {"appId":"wxe4137db8f68cabae","timeStamp":"1462346970","nonceStr":"2qqgnf39bcqb2a6hgyp0r6gc4to1rjan","package":"prepay_id=wx20160504152920c8c6a8ed6b0152391148","signType":"MD5","paySign":"3F964D9A1457C21B1413EC0113729D58"}
?>

<!DOCTYPE HTML>
<head>
    <title>微信jsApi支付</title>
    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,user-scalable=0" />
    <script type="text/javascript">
        //调用微信JS api 支付
        function jsApiCall()
        {
            WeixinJSBridge.invoke(
                'getBrandWCPayRequest',
                <?php echo $jsApiParameters; ?>,
                function(res){
                    WeixinJSBridge.log(res.err_msg);
                    if (res.err_msg == "get_brand_wcpay_request:ok") {
                        alert("支付成功");
                    } else {
                        alert("支付失败");
                    }
                }
            );
        }

        function callpay()
        {
            if (typeof WeixinJSBridge == "undefined"){
                if( document.addEventListener ){
                    document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
                }else if (document.attachEvent){
                    document.attachEvent('WeixinJSBridgeReady', jsApiCall);
                    document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
                }
            }else{
                jsApiCall();
            }
        }
    </script>
</head>
<body>
    <div align="center">
        <button type="button" onclick="callpay()" >开始支付</button>
    </div>
</body>
</html>
