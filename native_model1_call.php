<?php
/**
 * Native（原生）支付模式一demo
 * ====================================================
 * 模式一：商户按固定格式生成链接二维码，用户扫码后调微信
 * 会将productid和用户openid发送到商户设置的链接上，商户收到
 * 请求生成订单，调用统一支付接口下单提交到微信，微信会返回
 * 给商户prepayid。
 * 本例程对应的二维码由native_call_qrcode.php生成；
 * 本例程对应的响应服务为native_call.php;
 * 需要两者配合使用。
 */
include_once "./lib/WxPay.Api.php";

$nativeCall = new NativeCall();

$postdata = $GLOBALS['HTTP_RAW_POST_DATA'];
$nativeCall->saveData($postdata);

if ($nativeCall->checkSign() == FALSE) {
    $nativeCall->setReturnParameter('return_code', 'FAIL');
    $nativeCall->setReturnParameter('return_msg', '签名失败');
    return $nativeCall->returnXml();
}

$product_id = $nativeCall->getProductId();


$returnXml = $nativeCall->createXml();
//echo $returnXML;

