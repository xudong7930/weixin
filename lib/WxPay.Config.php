<?php
class WxPayConf
{
    const APPID = 'wxe4137db8f68cabae'; //APPID
    const APPSECRET = 'a87e70a3f0e2b4bcff050fd3e3cfb856'; //APPSECRET
    const MCHID = '1264718501'; //商户号
    const KEY = '2b92tgyKN2fRUmR4Ma7YfofoeE8Lmr7H'; //支付密钥

    //商户证书路径: 仅退款、撤销订单时需要
    const SSLCERT_PATH = './cert/apiclient_cert.pem';
    const SSLKEY_PATH = './cert/apiclient_key.pem';

    //默认CURL_PROXY_HOST=0.0.0.0和CURL_PROXY_PORT=0，此时不开启代理
    const CURL_PROXY_HOST = '0.0.0.0';
    const CURL_PROXY_PROT= 0;
    const CURL_TIMEOUT = 30;

    //上报信息配置: 0.关闭上报; 1.仅错误出错上报; 2.全量上报
    const REPORT_LEVEL = 1;

    //获取access_token过程中的跳转uri，通过跳转将code传入jsapi支付页面
    const JS_API_CALL_URL = 'http://m.xqian.cn/pay2/jsapi.php';

    //异步通知url
    const NOTIFY_URL = 'http://m.xqian.cn/pay2/notify.php';
}